import sys
import time
from baseclass.run import *
import mlflow
from mlflow.tracking import MlflowClient
from mlflow.utils import mlflow_tags
from mlflow.entities import RunStatus
from mlflow.utils.logging_utils import eprint
from mlflow.tracking.fluent import _get_experiment_id
from util.database.dao_postgres import crud_operations as dbClient
if __name__ == '__main__':
    project_name=sys.argv[7]
    experiment_name = project_name
    experiment = mlflow.get_experiment_by_name(experiment_name)
    experiment_id = None
    if experiment != None:
        print("INFO:Experiment found")
        experiment_id = experiment.experiment_id
    else:
        print("INFO : Experiment not found. Creating new experiment")
        experiment_id = mlflow.create_experiment(name=experiment_name)
        experiment = mlflow.get_experiment_by_name(experiment_name)
    client = mlflow.tracking.MlflowClient()
    run = client.create_run(experiment_id)
    print("INFO:-------RUN DETAILS------")
    print("\tExperiment id: {}".format(run.info.experiment_id))
    print("\tRun id: {}".format(run.info.run_id))
    print("\tlifecycle_stage: {}".format(run.info.lifecycle_stage))
    print("\tstatus: {}".format(run.info.status))
    print("\tRun tags: {}".format(run.data.tags))
    run_id="http://127.0.0.1:5000/#/experiments/0/runs/"+run.info.run_id
    print("\tRun tags: {}".format(run.info.run_id))
    s = time.time()

    try:
        file1=sys.argv[1]
        file2=sys.argv[2]
        input_data_source=sys.argv[3]
        result_file_name=sys.argv[4]
        s3_bucket_name=sys.argv[5]
        result_store=sys.argv[6]
        project_name=sys.argv[7]
        project_id=sys.argv[8]

        Run(file1,file2,input_data_source,result_file_name,s3_bucket_name,result_store,project_name).run()
        db_client=dbClient().insert_into_mlflowexec_list(project_id,True,run_id)

    except KeyError:
        print(traceback.format_exc())
        exit(-1)
    e = time.time()
    print("Run time: %f s" % (e - s))
