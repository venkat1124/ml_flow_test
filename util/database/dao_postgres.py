import sys,os, zlib,zipfile,shutil,json
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../../")
from util.database.database_connection import database_connection,meta,SessionLocal
import pandas as pd
from util.database.models import *
from sqlalchemy.exc import *
from sqlalchemy.schema import DropTable, CreateTable
from sqlalchemy import  Column,  Integer, String, Table,text,Float,Text,cast, Date
import numpy as np
import traceback
from sqlalchemy.orm import Session
class crud_operations:
    def __init__(self):
        self.cursor = None
    def insert_into_mlflowexec_list(self,project_id,flag,url):
        try :
            self.cursor = SessionLocal()
            result = self.cursor.query(model_monitoring).filter(model_monitoring.project_id == project_id).first()
            if result is None :
                insert = model_monitoring(project_id = project_id,flag = flag,mlflow_url = url)
                self.cursor.add(insert)
                self.cursor.commit()
            else :
                self.cursor.query(model_monitoring).filter(model_monitoring.project_id == project_id).update({'flag': flag,'mlflow_url':url})
                self.cursor.commit()
        except:
            print(traceback.format_exc())
            self.cursor.rollback()
        finally:
            self.cursor.close()