from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
import sys,os
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../../")
from util.database.database_connection import Base
class model_monitoring(Base):

    __tablename__="model_monitoring"

    id= Column(Integer,primary_key = True,index=True)

    project_id=Column(Integer,ForeignKey("project_list.id"))

    flag=Column(Boolean)

    mlflow_url = Column(String)

class Project_list(Base):
    __tablename__ = "project_list"

    id = Column(Integer, primary_key=True, index=True)
    project_name = Column(String, unique=True, index=True)
    project_description = Column(String)
    created_date = Column(String)
    updated_date = Column(String)
    dataset_name = Column(String)
    user_id = Column(Integer,ForeignKey("user_details.id"))