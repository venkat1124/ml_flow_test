import sys
import os,json
import traceback
import pandas as pd
from config.rules import *
class Run(object):
    def __init__(self,file1,file2,input_data_source,result_file_name,s3_bucket_name,result_store,project_name):
        self.file1=file1
        self.file2=file2
        self.input_data_source=input_data_source
        self.result_file_name=result_file_name
        self.s3_bucket_name=s3_bucket_name
        self.result_store=result_store
        self.project_name=project_name
    def getFiles(self):
        if self.input_data_source == "file":
            df1,df2 = self.getDataFromLocal()
        elif self.input_data_source == 's3':
            file_path = "datasets/"
            df1 = pd.read_csv("s3://"+self.s3_bucket_name+"/"+self.project_name+"/monitor/"+self.file1)
            df2 = pd.read_csv("s3://"+self.s3_bucket_name+"/"+self.project_name+"/monitor/"+self.file2)
        else:
            df1 = pd.DataFrame()
            df2 = pd.DataFrame()
        return df1,df2

    def getDataFromLocal(self):
        df1 = pd.read_csv(os.getcwd()+"/../datasets/"+self.file1)
        df2 = pd.read_csv(os.getcwd()+"/../datasets/"+self.file2)
        return df1,df2
    def run(self):
        df1,df2=self.getFiles()
        execute(df1,df2)

